<?php

use app\modules\credits\models\Credit;
use yii\bootstrap\Html;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $usersProvider ActiveDataProvider */
/* @var $creditsProvider ActiveDataProvider */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Кредит с низким процентом</h1>
        <p>
            <?= Html::a('Получить кредит', Yii::$app->user->isGuest ? ['/users/accounts/register'] : ['/credits/credits/request'], ['class' => 'btn btn-success']) ?>
            <?= Html::a('Сгенерировать аккаунты', ['/users/accounts/generate'], ['class' => 'btn btn-default']) ?>
            <?= Html::a('Сгенерировать кредиты', ['/credits/credits/generate'], ['class' => 'btn btn-default']) ?>
        </p>
    </div>

    <h3 class="text-muted">Пользователи</h3>
    <?= GridView::widget([
        'dataProvider' => $usersProvider,
        'columns' => [
            'id',
            'first_name',
            'last_name',
            'patronymic',
            'birthday:date',
            'passport_series',
            'passport_id',
            'email',
            'phone_number',
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>
    <br>
    <h3 class="text-muted">Заявки на оформление кредита</h3>
    <?= GridView::widget([
        'dataProvider' => $creditsProvider,
        'columns' => [
            'id',
            'user_id',
            'user.email',
            [
                'attribute' => 'amount',
                'value' => function (Credit $model) {
                    return $model->getAmount();
                },
            ],
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>
</div>
