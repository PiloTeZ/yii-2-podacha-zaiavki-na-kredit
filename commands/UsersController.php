<?php

namespace app\commands;

use app\modules\users\components\UserRegister;
use app\modules\users\models\UserRegisterData;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\Json;

class UsersController extends Controller
{
    public function actionCreate()
    {
        $model = new UserRegisterData();
        $register = new UserRegister($model);
        $model->first_name = $this->prompt('Имя', ['required' => true]);
        $model->last_name = $this->prompt('Фамилия', ['required' => true]);
        $model->patronymic = $this->prompt('Отчество', ['required' => true]);
        $model->birthday = $this->prompt('Дата рождения (yyyy-mm-dd)', ['required' => true]);
        $model->passport = $this->prompt('Паспорт', ['required' => true]);
        $model->email = $this->prompt('Email', ['required' => true]);
        $model->phone_number = $this->prompt('Телефон (+7 999 999 99 99)', ['required' => true]);

        if ($register->execute()) {
            $this->stdout("Пользователь добавлен\n");

            return ExitCode::OK;
        } else {
            $this->stdout("Ошибка при добавлении пользователя " . Json::encode($model->getErrors()) . "\n");

            return ExitCode::UNSPECIFIED_ERROR;
        }
    }
}
