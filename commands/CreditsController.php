<?php

namespace app\commands;

use app\modules\credits\components\CreditRequestCreate;
use app\modules\credits\models\CreditRequestData;
use app\modules\users\models\User;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\Json;

class CreditsController extends Controller
{
    public function actionRequest()
    {
        $model = new CreditRequestData();
        $model->user = User::findOne($this->prompt('ID пользователя', ['required' => true]));
        $model->amount = $this->prompt('Сумма', ['required' => true]);

        $request = new CreditRequestCreate($model);
        if ($request->execute()) {
            $this->stdout("Запрос на кредит добавлен\n");

            return ExitCode::OK;
        } else {
            $this->stdout("Во время добавления заявки произошла ошибка " . Json::encode($model->getErrors()) . "\n");

            return ExitCode::UNSPECIFIED_ERROR;
        }
    }
}
