<?php

namespace app\commands;

use app\modules\credits\components\CreditsGenerate;
use app\modules\credits\models\CreditsGenerateData;
use app\modules\users\components\UsersGenerate;
use app\modules\users\models\UsersGenerateData;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\Json;

class GenerateController extends Controller
{
    public function actionUsers()
    {
        $data = new UsersGenerateData();
        $data->number = $this->prompt('Количество записей', ['required' => true]);
        $generate = new UsersGenerate($data);
        if ($generate->execute()) {
            $this->stdout("Пользователи созданы\n");

            return ExitCode::OK;
        } else {
            $this->stdout("Ошибка при добавлении пользователей " . Json::encode($data->getErrors()) . "\n");

            return ExitCode::UNSPECIFIED_ERROR;
        }
    }

    public function actionCredits()
    {
        $data = new CreditsGenerateData();
        $data->number = $this->prompt('Количество записей', ['required' => true]);
        $generate = new CreditsGenerate($data);
        if ($generate->execute()) {
            $this->stdout("Кредиты созданы\n");

            return ExitCode::OK;
        } else {
            $this->stdout("Ошибка при добавлении кредитов " . Json::encode($data->getErrors()) . "\n");

            return ExitCode::UNSPECIFIED_ERROR;
        }
    }
}
