<?php

namespace app\helpers;

class AmountHelper
{
    public static function prepareToSave($amount)
    {
        return ceil($amount * 100);
    }

    public static function prepareToRead($amount)
    {
        return $amount / 100;
    }
}
