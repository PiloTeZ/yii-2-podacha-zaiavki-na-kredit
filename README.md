## Как развернуть проект?
Установить vagrant и virtualbox. Выполнить команду в корне проекта.
```
vagrant up
```

## Главная страница
http://yii2basic.test

## Консольные команды
```
php yii users/create
php yii credits/request
php yii generate/users
php yii generate/credits
```
