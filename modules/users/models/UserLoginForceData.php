<?php

namespace app\modules\users\models;

use app\interfaces\ComponentDataInterface;
use yii\base\Model;

class UserLoginForceData extends Model implements ComponentDataInterface
{
    /** @var User */
    public $user;

    public $isRemember = false;

    public function rules()
    {
        return [
            ['user', 'required'],
            ['isRemember', 'boolean'],
        ];
    }
}
