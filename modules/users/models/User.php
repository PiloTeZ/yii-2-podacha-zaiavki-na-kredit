<?php

namespace app\modules\users\models;

use app\modules\credits\models\Credit;
use DateTime;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $patronymic
 * @property string $birthday
 * @property string $passport_series
 * @property string $passport_id
 * @property string $email
 * @property string $phone_number
 * @property string $auth_key
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Credit[] $credits
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface
{
    const MIN_AGE = 18;

    const MAX_AGE = 60;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'patronymic', 'birthday', 'passport_series', 'passport_id', 'email', 'phone_number'], 'required'],
            [['first_name', 'last_name', 'patronymic'], 'match', 'pattern' => '/[А-я]+/i'],
            [['first_name', 'last_name', 'patronymic'], 'string', 'min' => 2, 'max' => 64],
            ['birthday', 'date', 'format' => 'php:Y-m-d'],
            ['birthday', function ($attribute, $params) {
                $datetime = new DateTime($this->$attribute);
                $datetimeCurrent = new DateTime();
                if ($datetime->diff($datetimeCurrent)->y < self::MIN_AGE) {
                    $this->addError($attribute, 'Минимальный возраст ' . self::MIN_AGE . ' лет');

                    return false;
                }

                return true;
            }],
            ['birthday', function ($attribute, $params) {
                $datetime = new DateTime($this->$attribute);
                $datetimeCurrent = new DateTime();
                if ($datetime->diff($datetimeCurrent)->y > self::MAX_AGE) {
                    $this->addError($attribute, 'Максимальный возраст ' . self::MAX_AGE . ' лет');
                }
            }],
            [['passport_series'], 'match', 'pattern' => '/\d{4}/i'],
            [['passport_id'], 'match', 'pattern' => '/\d{6}/i'],
            [['email'], 'email'],
            // Правила могут быть использованы в аналогичных классах, например формах, поэтому указан targetClass
            [['email', 'phone_number'], 'unique'],
            [['phone_number'], 'filter', 'filter' => function ($value) {
                return preg_replace('/[^\+\d]/', '', $value);
            }],
            ['phone_number', 'match', 'pattern' => '/\+7[\d]{10}/i'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'Имя',
            'last_name' => 'Фамилия',
            'patronymic' => 'Отчество',
            'birthday' => 'Дата рождения',
            'passport_series' => 'Серия паспорта',
            'passport_id' => 'Номер паспорта',
            'email' => 'Email',
            'phone_number' => 'Номер телефона',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCredits()
    {
        return $this->hasMany(Credit::class, ['user_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * {@inheritdoc}
     * @throws NotSupportedException
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }
}
