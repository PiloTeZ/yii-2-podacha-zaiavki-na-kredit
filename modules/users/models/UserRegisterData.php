<?php

namespace app\modules\users\models;

use app\interfaces\ComponentDataInterface;

class UserRegisterData extends User implements ComponentDataInterface
{
    public $first_name;

    public $last_name;

    public $patronymic;

    public $birthday;

    public $passport;

    public $email;

    public $phone_number;

    public $authorize = false;

    public function scenarios()
    {
        return [
            static::SCENARIO_DEFAULT => $this->attributes(),
        ];
    }

    /**
     * Ограничение набора полей.
     * Модель наследуется от User, что бы не дублировать правила валидации, но остальные поля нам не нужны, поэтому
     * набор полей зафиксирован
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'first_name',
            'last_name',
            'patronymic',
            'birthday',
            'passport',
            'email',
            'phone_number',
        ];
    }

    public function rules()
    {
        return array_merge(parent::rules(), [
            [['passport'], 'filter', 'filter' => function ($value) {
                return preg_replace('/[^\d]/', '', $value);
            }],
            ['passport', 'match', 'pattern' => '/\d{10}/'],
            ['authorize', 'boolean'],
        ]);
    }

    public function getPassportSeries()
    {
        if (!$this->validate('passport')) {
            return null;
        }

        return mb_substr($this->passport, 4, 4);
    }

    public function getPassportId()
    {
        if (!$this->validate('passport')) {
            return null;
        }

        return mb_substr($this->passport, 0, 6);
    }

    public function attributeLabels()
    {
        return array_merge([
            'passport' => 'Серия и номер паспорта',
        ], parent::attributeLabels());
    }
}
