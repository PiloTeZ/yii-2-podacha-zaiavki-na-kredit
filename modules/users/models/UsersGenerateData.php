<?php

namespace app\modules\users\models;

use app\interfaces\ComponentDataInterface;
use yii\base\Model;

class UsersGenerateData extends Model implements ComponentDataInterface
{
    public $number = 10;

    public function rules()
    {
        return [
            ['number', 'required'],
            ['number', 'integer'],
        ];
    }
}
