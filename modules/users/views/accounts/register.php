<?php

use yii\helpers\Html;
use yii\jui\DatePicker;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\users\models\User */
/* @var $form ActiveForm */

$datepicker = DatePicker::widget([
    'language' => 'ru',
    'dateFormat' => 'dd.MM.yyyy',
    'options' => [
        'placeholder' => 'ДД.ММ.ГГГГ',
        'class' => 'form-control',
        'autocomplete' => 'off',
    ],
    'clientOptions' => [
        'altField' => '#' . Html::getInputId($model, 'birthday'),
        'altFormat' => 'yy-mm-dd',
        'changeMonth' => true,
        'changeYear' => true,
        'yearRange' => date('Y') - 100 . ':' . date('Y'),
    ]]);
?>
<div class="register">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'first_name') ?>
    <?= $form->field($model, 'last_name') ?>
    <?= $form->field($model, 'patronymic') ?>
    <?= $form->field($model, 'birthday', ['template' => "{label}\n$datepicker\n{input}\n{hint}\n{error}", 'inputOptions' => ['class' => 'hidden']]) ?>
    <?= $form->field($model, 'passport') ?>
    <?= $form->field($model, 'email') ?>
    <?= $form->field($model, 'phone_number', ['options' => ['placeholder' => '+7 (999) 999 99 99']]) ?>

    <div class="form-group">
        <?= Html::submitButton('Зарегистрироваться', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div><!-- register -->
