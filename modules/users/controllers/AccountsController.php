<?php

namespace app\modules\users\controllers;

use app\modules\users\components\UserRegister;
use app\modules\users\components\UsersGenerate;
use app\modules\users\models\UserRegisterData;
use app\modules\users\models\UsersGenerateData;
use Yii;
use yii\filters\AccessControl;

class AccountsController extends \yii\web\Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }

    public function actionRegister()
    {
        $session = Yii::$app->session;
        $model = new UserRegisterData();
        $model->authorize = true;

        if ($model->load(Yii::$app->request->post())) {
            if (!$model->validate()) {
                $session->addFlash('danger', 'Форма заполнена неверно');
            } else {
                $register = new UserRegister($model);
                if ($register->execute()) {
                    $session->addFlash('success', 'Вы успешно зарегистрированы');

                    return $this->redirect(['/credits/credits/request']);
                } else {
                    $session->addFlash('danger', 'Во время регистрации произошла ошибка');
                }
            }
        }

        return $this->render('register', compact('model'));
    }

    public function actionGenerate($number = 10)
    {
        $data = new UsersGenerateData();
        $data->number = $number;
        $generate = new UsersGenerate($data);
        if ($generate->execute()) {
            Yii::$app->session->addFlash('success', 'Добавлено ' . $number . ' пользователей');
        } else {
            Yii::$app->session->addFlash('danger', 'Ошибка при генерации');
        }

        return $this->redirect('/');
    }
}
