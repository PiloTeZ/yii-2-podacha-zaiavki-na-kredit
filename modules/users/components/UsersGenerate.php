<?php

namespace app\modules\users\components;

use app\components\BaseComponent;
use app\interfaces\ComponentDataInterface;
use app\modules\users\models\UserRegisterData;
use app\modules\users\models\UsersGenerateData;
use DateTime;
use yii\helpers\Inflector;

/**
 * @property UsersGenerateData $data
 */
class UsersGenerate extends BaseComponent
{
    public function __construct(ComponentDataInterface $data, array $config = [])
    {
        parent::__construct($data, $config);
    }

    /**
     * @throws \Exception
     */
    protected function executeInternal()
    {
        for ($i = 1; $i <= $this->data->number; $i++) {
            $userData = new UserRegisterData();
            $userData->first_name = $this->generateName($this->getFirstNames());
            $userData->last_name = $this->generateName($this->getLastNames());
            $userData->patronymic = $this->generateName($this->getPatronymics());
            $userData->birthday = $this->generateBirthday()->format('Y-m-d');
            $userData->passport = $this->generatePassport();
            $userData->email = $this->generateEmail($userData->last_name);
            $userData->phone_number = $this->generatePhoneNumber();
            (new UserRegister($userData))->execute();
        }

        return true;
    }

    private function getFirstNames()
    {
        return [
            'Вася',
            'Антон',
            'Петя',
            'Боря',
        ];
    }

    private function getLastNames()
    {
        return [
            'Фордов',
            'Сяоми',
            'Кадилакин',
            'Ниссан',
        ];
    }

    private function getPatronymics()
    {
        return [
            'Петрович',
            'Антонович',
            'Василиевич',
            'Борисович',
        ];
    }

    /**
     * @return DateTime
     * @throws \Exception
     */
    private function generateBirthday()
    {
        $date = new DateTime();
        $date->modify('-' . mt_rand(18, 60) . ' year');

        return $date;
    }

    private function generateName(array $names)
    {
        return $names[array_rand($names)];
    }

    private function generatePassport()
    {
        return mt_rand(1000, 9999) . mt_rand(100000, 999999);
    }

    private function generateEmail($loginPrefix)
    {
        $prefixFormatted = Inflector::camel2id($loginPrefix);
        $prefixFormatted = Inflector::transliterate($prefixFormatted);

        return $prefixFormatted . '-' . mt_rand(100000, 9999999) . '@ya.ru';
    }

    private function generatePhoneNumber()
    {
        return '+7' . mt_rand(100, 999) . mt_rand(100, 999) . mt_rand(1000, 9999);
    }
}
