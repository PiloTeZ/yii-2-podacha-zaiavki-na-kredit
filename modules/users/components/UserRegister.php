<?php

namespace app\modules\users\components;

use app\components\BaseComponent;
use app\modules\users\models\User;
use app\modules\users\models\UserLoginForceData;
use app\modules\users\models\UserRegisterData;
use Yii;
use yii\helpers\Json;

/**
 * @property UserRegisterData $data
 */
class UserRegister extends BaseComponent
{
    /**
     * @return bool
     * @throws \yii\base\Exception
     */
    public function executeInternal()
    {
        $user = new User();
        $user->first_name = $this->data->first_name;
        $user->last_name = $this->data->last_name;
        $user->patronymic = $this->data->patronymic;
        $user->birthday = $this->data->birthday;
        $user->passport_series = $this->data->getPassportSeries();
        $user->passport_id = $this->data->getPassportId();
        $user->email = $this->data->email;
        $user->phone_number = $this->data->phone_number;
        $user->auth_key = Yii::$app->security->generateRandomString(32);

        $result = $user->save();

        if (!$result) {
            return false;
        }

        if ($this->data->authorize) {
            $this->authorize($user);
        }

        return $result;
    }

    /**
     * @param User $user
     */
    private function authorize(User $user)
    {
        $loginData = new UserLoginForceData();
        $loginData->user = $user;
        $loginData->isRemember = true;
        $login = new UserLoginForce($loginData);
        // Успех авторизации не фаталит результат, так как регистрация прошла успешно и это главное
        if (!$login->execute()) {
            Yii::error('Не удалось авторизовать нового зарегистрированного пользователя ' . Json::encode($user->id), __METHOD__);
        }
    }
}
