<?php

namespace app\modules\users\components;

use app\components\BaseComponent;
use app\modules\users\models\UserLoginForceData;
use Yii;

/**
 * @property UserLoginForceData $data
 */
class UserLoginForce extends BaseComponent
{
    /** @var int Длительность авторизации в секундах (год) */
    const REMEMBER_DURATION = 31104000;

    static public function isAvailable()
    {
        return Yii::$app->user->isGuest;
    }

    /**
     * @see execute()
     */
    protected function executeInternal()
    {
        if (!static::isAvailable()) {
            return false;
        }

        return Yii::$app->user->login($this->data->user, $this->data->isRemember ? static::REMEMBER_DURATION : 0);
    }
}
