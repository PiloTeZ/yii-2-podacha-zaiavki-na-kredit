<?php

namespace app\modules\credits\models;

use app\interfaces\ComponentDataInterface;
use app\modules\users\models\User;
use yii\base\Model;

class CreditRequestData extends Model implements ComponentDataInterface
{
    /** @var float */
    public $amount;

    /** @var User */
    public $user;

    /** @var string */
    public $ip;

    /** @var string */
    public $userAgent;

    public function scenarios()
    {
        return [
            static::SCENARIO_DEFAULT => [
                'amount',
            ],
        ];
    }

    public function rules()
    {
        return [
            [['user', 'amount'], 'required'],
            [['amount'], 'number'],
            [['userAgent'], 'string'],
            [['ip'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'amount' => 'Сумма руб.',
        ];
    }
}
