<?php

namespace app\modules\credits\models;

use app\interfaces\ComponentDataInterface;
use yii\base\Model;

class CreditsGenerateData extends Model implements ComponentDataInterface
{
    public $number = 10;

    public function rules()
    {
        return [
            ['number', 'required'],
            ['number', 'integer'],
        ];
    }
}
