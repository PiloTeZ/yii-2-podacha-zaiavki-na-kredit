<?php

namespace app\modules\credits\models;

use app\helpers\AmountHelper;
use app\modules\users\models\User;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "credits".
 *
 * @property int $id
 * @property int $user_id
 * @property int $amount_int
 * @property int $percent
 * @property int $status
 * @property string $user_agent
 * @property string $ip
 * @property int $created_at
 * @property int $updated_at
 *
 * @property User $user
 */
class Credit extends \yii\db\ActiveRecord
{
    /** @var int Кредит на рассмотрении */
    const STATUS_REQUEST = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'credits';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'amount_int', 'percent'], 'required'],
            [['user_id', 'amount_int', 'percent'], 'integer'],
            [['user_agent'], 'string'],
            [['ip'], 'string', 'max' => 64],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'amount' => 'Сумма руб.',
            'user_agent' => 'User Agent',
            'ip' => 'Ip',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения',
        ];
    }

    public function getAmount()
    {
        return AmountHelper::prepareToRead($this->amount_int);
    }

    public function setAmount($amount)
    {
        $this->amount_int = AmountHelper::prepareToSave($amount);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
}
