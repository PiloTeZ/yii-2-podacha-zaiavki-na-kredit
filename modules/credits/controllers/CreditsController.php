<?php

namespace app\modules\credits\controllers;

use app\modules\credits\components\CreditRequestCreate;
use app\modules\credits\models\CreditRequestData;
use app\modules\credits\models\CreditsGenerateData;
use app\modules\credits\components\CreditsGenerate;
use Yii;
use yii\filters\AccessControl;

class CreditsController extends \yii\web\Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['generate'],
                        'allow' => true,
                    ],
                ],
            ],
        ];
    }

    public function actionRequest()
    {
        $session = Yii::$app->session;
        $model = new CreditRequestData();
        $model->user = Yii::$app->user->identity;
        $model->userAgent = Yii::$app->request->userAgent;
        $model->ip = Yii::$app->request->userIP;

        if ($model->load(Yii::$app->request->post())) {
            if (!$model->validate()) {
                $session->addFlash('danger', 'Форма заполнена неверно');
            } else {
                $request = new CreditRequestCreate($model);
                if ($request->execute()) {
                    $session->addFlash('success', 'Заявка на оформление кредита успещно подана. Результат рассмотрения будет отправлен на email');

                    return $this->redirect('/');
                } else {
                    $session->addFlash('danger', 'Во время оформления заявки произошла ошибка');
                }
            }
        }

        return $this->render('request', compact('model'));
    }

    public function actionGenerate($number = 10)
    {
        $data = new CreditsGenerateData();
        $data->number = $number;
        $generate = new CreditsGenerate($data);
        if ($generate->execute()) {
            Yii::$app->session->addFlash('success', 'Добавлено ' . $number . ' заявок на кредиты');
        } else {
            Yii::$app->session->addFlash('danger', 'Ошибка при генерации');
        }

        return $this->redirect('/');
    }
}
