<?php

namespace app\modules\credits\components;

use app\components\BaseComponent;
use app\interfaces\ComponentDataInterface;
use app\modules\credits\models\CreditRequestData;
use app\modules\credits\models\CreditsGenerateData;
use app\modules\users\models\User;

/**
 * @property CreditsGenerateData $data
 */
class CreditsGenerate extends BaseComponent
{
    public function __construct(ComponentDataInterface $data, array $config = [])
    {
        parent::__construct($data, $config);
    }

    /**
     * @throws \Exception
     */
    protected function executeInternal()
    {
        for ($i = 1; $i <= $this->data->number; $i++) {
            $requestData = new CreditRequestData();
            $requestData->amount = $this->generateAmount();
            $requestData->user = $this->generateUser();
            $requestData->ip = $this->generateIp();
            $requestData->userAgent = $this->generateUserAgent();
            (new CreditRequestCreate($requestData))->execute();
        }

        return true;
    }

    private function generateAmount()
    {
        return mt_rand(1000, 1000000) . '.' . mt_rand(00, 99);
    }

    private function generateUser()
    {
        $users = User::find()->orderBy(['id' => SORT_DESC])->limit(100)->all();

        return $users[array_rand($users)];
    }

    private function generateIp()
    {
        return mt_rand(100, 999) . '.' . mt_rand(100, 999) . '.' . mt_rand(100, 999) . '.' . mt_rand(100, 999);
    }

    private function generateUserAgent()
    {
        $agents = [
            'Chrome fox from village home dogs',
            'Kadilaki tini kandelaki',
        ];

        return $agents[array_rand($agents)];
    }
}
