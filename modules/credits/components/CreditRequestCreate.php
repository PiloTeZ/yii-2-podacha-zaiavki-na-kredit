<?php

namespace app\modules\credits\components;

use app\components\BaseComponent;
use app\modules\credits\models\Credit;
use app\modules\credits\models\CreditRequestData;
use Yii;

/**
 * @property CreditRequestData $data
 */
class CreditRequestCreate extends BaseComponent
{
    public function executeInternal()
    {
        $creditPercentDefault = Yii::$app->params['credits']['percent'] ?? null;
        if (!$creditPercentDefault) {
            Yii::error('Не установлен процент кредита по умолчанию', __METHOD__);

            return false;
        }

        $credit = new Credit();
        $credit->user_id = $this->data->user->id;
        $credit->status = Credit::STATUS_REQUEST;
        $credit->setAmount($this->data->amount);
        $credit->percent = $creditPercentDefault;
        $credit->user_agent = $this->data->userAgent;
        $credit->ip = $this->data->ip;

        return $credit->save();
    }
}
