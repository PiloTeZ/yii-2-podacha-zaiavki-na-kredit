<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\credits\models\Credit */
/* @var $form ActiveForm */
?>
<div class="request">

    <?php $form = ActiveForm::begin(['method' => 'post']); ?>

        <?= $form->field($model, 'amount') ?>
    
        <div class="form-group">
            <?= Html::submitButton('Запросить кредит', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- request -->
