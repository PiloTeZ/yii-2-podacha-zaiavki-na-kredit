<?php

use yii\db\Migration;

/**
 * Class m190927_131519_create_table_users
 */
class m190927_131519_create_table_users extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // TODO Придумать что-нибудь получше, например вынести в базовый класс
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'first_name' => $this->string(128)->notNull(),
            'last_name' => $this->string(128)->notNull(),
            'patronymic' => $this->string(128)->notNull(),
            'birthday' => $this->date()->notNull(),
            'passport_series' => $this->string(4)->notNull(),
            'passport_id' => $this->string(6)->notNull(),
            'email' => $this->string()->notNull(),
            'phone_number' => $this->string(16)->notNull(),
            'auth_key' => $this->string(32)->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
    }
}
