<?php

use yii\db\Migration;

/**
 * Class m190927_131550_create_table_credits
 */
class m190927_131550_create_table_credits extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // TODO Придумать что-нибудь получше, например вынести в базовый класс
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('credits', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            // До 99 999 999,00
            'amount_int' => $this->integer(10)->notNull(),
            'percent' => $this->integer(2)->notNull(),
            'status' => $this->tinyInteger(1)->notNull(),
            'user_agent' => $this->text(),
            'ip' => $this->string(64),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addForeignKey('credits_user_id_fk', 'credits', 'user_id', 'users', 'id', 'RESTRICT', 'RESTRICT');
    }
}
