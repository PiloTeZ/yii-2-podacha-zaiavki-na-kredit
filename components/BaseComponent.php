<?php

namespace app\components;

use app\interfaces\ComponentDataInterface;
use yii\base\BaseObject;

abstract class BaseComponent extends BaseObject
{
    /**
     * @var ComponentDataInterface
     */
    protected $data;

    public function __construct(ComponentDataInterface $data, array $config = [])
    {
        parent::__construct($config);
        $this->data = $data;
    }

    final public function execute()
    {
        // TRICKY Перед выполнением должны валидироваться все параметры, а не только разрешенные в сценарии
        // При получении данных из интерфейса, достаточно проверить только введенные параметры (то есть не передавать аттрибуты в validate)
        if (!$this->data->validate($this->data->attributes())) {
            return false;
        }

        return $this->executeInternal();
    }

    abstract protected function executeInternal();
}
