<?php

namespace app\interfaces;

interface ComponentDataInterface
{
    public function validate($attributes = []);
    public function attributes();
}
